package edu.cvtc.java.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.cvtc.java.Cuboid;

public class CuboidTest {
	
	// setting values for additional testing


	Cuboid cube1 = new Cuboid(1.0f, 1.0f, 1.0f);
	Cuboid cube2 = new Cuboid(2.0f, 2.0f, 2.0f);
	Cuboid cube3 = new Cuboid(3.0f, 3.0f, 3.0f);

	
	// Testing the arguments

	@Test
	public void testSurfaceArea1() {
		assertEquals(6.0f, cube1.surfaceArea(), 0.0);
	}

	@Test
	public void testVolume1() {
		assertEquals(1.0f, cube1.volume(), 0.0);
	}
	
	@Test
	public void testSurfaceArea2() {
		assertEquals(48.0f, cube2.surfaceArea(), 0.0);
	}

	@Test
	public void testVolume2() {
		assertEquals(8.0f, cube2.volume(), 0.0);
	}
	
	@Test
	public void testSurfaceArea3() {
		assertEquals(6.0f, cube1.surfaceArea(), 0.0);
	}

	@Test
	public void testVolume3() {
		assertEquals(1.0f, cube1.volume(), 0.0);
	}
	
	@Test 
	public void testWidth() {
		assertEquals(1.0f, cube1.getWidth(), 0.0);
	}
	
	@Test
	public void testHeight() {
		assertEquals(1.0f, cube1.getHeight(), 0.0);
	}
	
	@Test
	public void testDepth() {
		assertEquals(1.0f, cube1.getDepth(), 0.0);
	}

}
