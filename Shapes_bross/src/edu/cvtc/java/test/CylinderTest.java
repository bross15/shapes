package edu.cvtc.java.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.cvtc.java.Cylinder;

public class CylinderTest {
	
	// setting values for additional testing


	Cylinder cylinder1 = new Cylinder(1.0f, 1.0f);
	Cylinder cylinder2 = new Cylinder(2.0f, 2.0f);
	Cylinder cylinder3 = new Cylinder(3.0f, 3.0f);
	
	
	// Testing the arguments

	@Test
	public void testSurfaceArea1() {
		assertEquals(12.56f, cylinder1.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume1() {
		assertEquals(2.0f, cylinder1.volume(), 0.0);
	}
	
	@Test
	public void testSurfaceArea2() {
		assertEquals(50.26f, cylinder2.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume2() {
		assertEquals(16.0f, cylinder2.volume(), 0.0);
	}
	
	@Test
	public void testSurfaceArea3() {
		assertEquals(113.09f, cylinder3.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume3() {
		assertEquals(54.0f, cylinder3.volume(), 0.0);
	}
	
	@Test 
	public void testHeight() {
		assertEquals(1.0f, cylinder1.getHeight(), 0.0);
	}
	
	@Test
	public void testRadius() {
		assertEquals(1.0f, cylinder1.getRadius(), 0.0);
	}
	

}
