package edu.cvtc.java.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.cvtc.java.Sphere;

public class SphereTest {

	// setting values for additional testing
	
	Sphere sphere1 = new Sphere(1.0f);
	Sphere sphere2 = new Sphere(2.0f);
	Sphere sphere3 = new Sphere(3.0f);
	
	
	// Testing the arguments
	@Test
	public void testSurfaceArea1() {
		assertEquals(12.56f, sphere1.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume1() {
		assertEquals(4.18f, sphere1.volume(), 0.1);
	}
	
	@Test
	public void testSurfaceArea2() {
		assertEquals(50.26f, sphere2.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume2() {
		assertEquals(33.51f, sphere2.volume(), 0.1);
	}
	
	@Test
	public void testSurfaceArea3() {
		assertEquals(113.09f, sphere3.surfaceArea(), 0.1);
	}

	@Test
	public void testVolume3() {
		assertEquals(113.09f, sphere3.volume(), 0.1);
	}
	
	@Test
	public void testRadius() {
		assertEquals(1.0f, sphere1.getRadius(), 0.0);
	}

}
