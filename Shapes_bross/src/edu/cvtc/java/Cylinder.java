package edu.cvtc.java;

import javax.swing.JOptionPane;

public class Cylinder extends Shape{

	
	private float radius; // creating the radius field
	private float height; // creating the height field
	
	// constructor for assigning parameters
	public Cylinder(float radius, float height) {
		this.radius = radius;
		this.height = height;
	}
	

	public float getRadius() { // to get the radius variable the user entered
		return radius;
	}
	public void setRadius(float radius) { // to set the radius from the get to the variable called radius
		this.radius = radius;
	}
	public float getHeight() {// to get the height variable the user entered
		return height;
	}
	public void setHeight(float height) { // to set the height from the get to the variable called height
		this.height = height;
	}
	
	// Creating the toString for each of the fields
	@Override
	public String toString() {
		return "Cylinder [radius=" + radius + ", height=" + height + "]";
	}
	
	
	// abstract methods from shape
	@Override
	public float surfaceArea() {
		
		// 2 * pi* r^2 formula for the surface area of a Cylinder
		
		return (float) ((2 * Math.PI * Math.pow(this.getRadius(), 2)) + (2 * Math.PI * this.getRadius() * this.getHeight()) );
	}
	
	@Override
	public float volume() {
		
		// 2 * r^2 * h formula for the volume of a Cylinder
		
		return (float) (2 * Math.pow(this.getRadius(), 2) * this.getHeight());
		
	}
	
	@Override
	public void render() {
		
		// Creates a pop-up message telling the user what the end values are in a sentence
		
		JOptionPane.showMessageDialog( null, "Surface Area of a Cylinder: " + surfaceArea() + " Volume of a Cylinder: " + volume() + ".");
		
	} 
	
	
	
	
}
