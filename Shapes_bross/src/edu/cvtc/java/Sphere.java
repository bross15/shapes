package edu.cvtc.java;

import javax.swing.JOptionPane;

public class Sphere extends Shape{

	private float radius; // creating the radius field
	
	// constructor for assigning parameters
	public Sphere(float radius) {
		this.radius = radius;
	}
	

	public float getRadius() { // to get the radius variable the user entered
		return radius;
	}

	public void setRadius(float radius) { // to set the radius from the get to the variable called radius
		this.radius = radius;
	}

	// Creating the toString for each of the fields
	@Override
	public String toString() {
		return "Sphere [radius=" + radius + "]";
	}

	
	
	// abstract methods from shape
	@Override
	public float surfaceArea() {
		return (float) (4 * Math.PI * Math.pow(this.getRadius(), 2)); // 4 * pi * r^2 formula for the surface area of a sphere
	}

	@Override
	public float volume() {
		return (float) (4.0 / 3.0 * Math.PI * Math.pow(this.getRadius(), 3)); // 4/3 * pi * r^3 formula for the volume of a sphere
	}

	@Override
	public void render() {
		
		// Creates a pop-up message telling the user what the end values are in a sentence
		
		JOptionPane.showMessageDialog( null, "Surface Area of a Sphere: " + surfaceArea() + " Volume of a Sphere: " + volume() + ".");
		
	}
	
	
	
}
