package edu.cvtc.java;

import javax.swing.JOptionPane;

public class Cuboid extends Shape{

	private float width; // creating the width field
	private float height; // creating the height field
	private float depth; // creating the depth field
	
	
	// constructor for assigning parameters
	public Cuboid(float width, float height, float depth) {
		this.width = width;
		this.height = height;
		this.depth = depth;
	}
	
	public float getWidth() { // to get the width variable the user entered
		return width;
	}
	public void setWidth(float width) { // to set the width from the get to the variable called width
		this.width = width;
	}
	public float getHeight() { // to get the height variable the user entered
		return height;
	}
	public void setHeight(float height) { // to set the height from the get to the variable called height
		this.height = height;
	}
	public float getDepth() { // to get the depth variable the user entered
		return depth;
	}
	public void setDepth(float depth) { // to set the depth from the get to the variable called depth
		this.depth = depth;
	}
	
	// Creating the toString for each of the fields
	@Override
	public String toString() {
		return "Cuboid [width=" + width + ", height=" + height + ", depth=" + depth + "]";
	}
	
	
	// abstract methods from shape
	@Override
	public float surfaceArea() {
		
		// 6 * w * h * d formula for the surface area of a Cube
		
		return (float) (6 * this.getWidth() * this.getHeight() * this.getDepth());
	}
	
	@Override
	public float volume() {
		
		// w * h * d formula for the volume of a Cube
		
		return (float) (this.getWidth() * this.getHeight() * this.getDepth());
	}
	
	@Override
	public void render() {
		
		// Creates a pop-up message telling the user what the end values are in a sentence
		
		JOptionPane.showMessageDialog( null, "Surface Area of a Cube: " + surfaceArea() + " Volume of a Cube: " + volume() + ".");
	}
	

	
	
	
	
	
}
