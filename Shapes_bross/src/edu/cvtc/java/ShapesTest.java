package edu.cvtc.java;

public class ShapesTest {

	public static void main(String[] args) {
		
		Shape cuboid = new Cuboid(10, (float) 5.9, 10); // creates a new instantiation of the class to grab methods of the class
		
		cuboid.render(); // calling the render method
		
		Shape cylinder = new Cylinder(5, 9); // creates a new instantiation of the class to grab methods of the class
		
		cylinder.render(); // calling the render method
		
		Shape sphere = new Sphere((float) 8.9); // creates a new instantiation of the class to grab methods of the class
		
		sphere.render(); // calling the render method
	}

	
}
