package edu.cvtc.java;

public abstract class Shape {
	
	// creating an abstract float methods                                                                         to have in all three shapes for each formula

	public abstract float surfaceArea(); 
	
	public abstract float volume();
	
	public abstract void render();
}
